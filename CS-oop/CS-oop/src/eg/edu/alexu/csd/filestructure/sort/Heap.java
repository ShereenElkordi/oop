package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Heap<T extends Comparable<T>> implements IHeap<T> {
	private int size = 0;
	private ArrayList<INode<T>> heap;

	@Override
	public INode<T> getRoot() {
		if (size > 1) {
			return heap.get(1);
		}
		return null;
	}

	@Override
	public int size() {
		if (size <= 1) {
			return 0;
		}
		return size - 1;
	}


	@Override
	public void heapify(INode<T> node) {
		INode<T> left = null;
		INode<T> right = null;
		INode<T> largest = null;
		if (((HeapNode) (node)).getIndex() * 2 < size && ((HeapNode) (node)).getIndex() * 2 + 1 < size) {
			left = heap.get(((HeapNode) (node)).getIndex() * 2);
			right = heap.get(((HeapNode) (node)).getIndex() * 2 + 1);
			largest = (left.getValue().compareTo(right.getValue()) > 0) ? left : right;
		} else if (((HeapNode) (node)).getIndex() * 2 < size) {
			left = heap.get(((HeapNode) (node)).getIndex() * 2);
			largest = left;

		}
		if (largest != null) {
			if (node.getValue().compareTo(largest.getValue()) < 0) {
				T temp = largest.getValue();
				largest.setValue(node.getValue());
				node.setValue(temp);
				heapify(largest);
			}
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public T extract() {
		if (size == 2) {
			size = 0;
			return heap.get(1).getValue();
		}
		if (size > 1) {
			T max = heap.get(1).getValue();
			heap.get(1).setValue(heap.get(size - 1).getValue());
			size--;
			heapify(getRoot());
			return max;

		}
		return null;
	}

	@Override
	public void insert(T element) {
		if (size > 1) {
			INode<T> child = new HeapNode();
			child.setValue(element);
			((HeapNode) child).setIndex(size);
			heap.add(size, child);
			size++;
			INode<T> parent = heap.get(((HeapNode) child).getIndex() / 2);
			while (parent != null && parent.getValue().compareTo(child.getValue()) < 0) {
				T temp = parent.getValue();
				parent.setValue(child.getValue());
				child.setValue(temp);
				child = parent;
				parent = heap.get(((HeapNode) child).getIndex() / 2);
			}

		} else {
			heap = new ArrayList<INode<T>>();
			heap.add(null);
			heap.add(new HeapNode());
			(heap.get(1)).setValue(element);
			((HeapNode) (heap.get(1))).setIndex(1);

			size = 2;
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void build(Collection<T> unordered) {
		heap = new ArrayList();
		heap.add(null);

		int i = 1;
		for (Iterator iterator = unordered.iterator(); iterator.hasNext();) {
			@SuppressWarnings("unchecked")
			T t = (T) iterator.next();
			heap.add(new HeapNode<>());
			heap.get(i).setValue(t);
			((HeapNode) heap.get(i)).setIndex(i++);
		}
		if (i > 1) {
			size = unordered.size() + 1;
		}
		for (i = size / 2; i > 0; i--) {
			heapify(heap.get(i));
		}
	}
	public void print(){
		for(int i=1;i<heap.size();i++){
			System.out.println(heap.get(i) + " ");
		}
	}

}
