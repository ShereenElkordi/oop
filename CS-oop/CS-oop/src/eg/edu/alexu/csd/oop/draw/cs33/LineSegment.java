package eg.edu.alexu.csd.oop.draw.cs33;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.HashMap;

/**
 * @author omid
 *
 */
public class LineSegment extends MyShape {

	public LineSegment() {
		this.properties = new HashMap<>();
		this.properties.put("endx", 0.0);
		this.properties.put("endy", 0.0);
	}

	@Override
	public void draw(Graphics canvas) {

		((Graphics2D) canvas).setStroke(new BasicStroke(2));
		((Graphics2D) canvas).setColor(getColor());
		((Graphics2D) canvas).drawLine((int) this.position.getX(), (int) this.position.getY(),
				this.properties.get("endx").intValue(), this.properties.get("endy").intValue());
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		MyShape r = new LineSegment();
		r.setColor(this.color);
		r.setFillColor(this.fillcolor);
		r.setPosition(this.position);
		r.setProperties(this.properties);
		return r;
	}

}
