package eg.edu.alexu.csd.oop.calculator.cs33;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.LinkedList;

import eg.edu.alexu.csd.oop.calculator.Calculator;

/**
 * @author omid
 *
 */
public class CalculatorImp implements Calculator {
	/**
	 * first number.
	 *
	 */
	private double firstOperand;
	/**
	 * second number.
	 *
	 */
	private double secondOperand;
	/** required operation. */
	private char operation;
	/** history. */
	private LinkedList<String> recentOperations =
			new LinkedList<String>();
	/** current operation. */
	private int current;
	/** max size. */
	private static final int MAX = 5;
	/** the current when I have saved. */
	private int currentSaved;

	/** Take input from user. */
	@Override
	public final void input(final String s) {
		recentOperations.add(s);
		if (recentOperations.size() > MAX) {
			recentOperations.removeFirst();
		}
		current = recentOperations.size() - 1;
	}

	/*
	 * Return the result of the current operations
	 * or throws a runtime exception.
	 */
	@Override
	public final String getResult() {
		String s = current();
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '+'
					|| s.charAt(i) == '-'
					|| s.charAt(i) == '*'
					|| s.charAt(i) == '/') {
				firstOperand = Double.parseDouble(
						s.substring(0, i));
				operation = s.charAt(i);
				secondOperand = Double.parseDouble(
						s.substring(i + 1));
				break;
			}
		}
		if (operation == '+') {
			return Double.toString(
					firstOperand + secondOperand);
		} else if (operation == '-') {
			return Double.toString(
					firstOperand - secondOperand);
		} else if (operation == '*') {
			return Double.toString(
					firstOperand * secondOperand);
		} else if (operation == '/') {
			if (secondOperand == 0) {
				throw new RuntimeException();
			} else {
				return Double.toString(
						firstOperand / secondOperand);
			}
		} else {
			throw new RuntimeException();
		}
	}

	/** return the current formula. */
	@Override
	public final String current() {
		if (current >= recentOperations.size()) {
			current = recentOperations.size() - 1;
		}
		if (recentOperations.size() != 0) {
			return recentOperations.get(current);
		} else {
			return null;
		}
	}

	/*
	 * return the next operation in String format,
	 *  or Null if no more history available.
	 */
	@Override
	public final String prev() {
		if (current >= recentOperations.size()) {
			current = recentOperations.size() - 1;
		}
		if (recentOperations.size() == 0) {
			return null;
		}
		if (current > 0) {
			current--;
			return recentOperations.get(current);
		} else {
			return null;
		}
	}

	/** Save in file the last 5 done Operations. */
	@Override
	public final String next() {
		if (current >= recentOperations.size()) {
			current = recentOperations.size() - 1;
		}
		if (recentOperations.size() == 0
				|| current == recentOperations.size() - 1) {
			return null;
		} else {
			current++;
			return recentOperations.get(current);
		}
	}

	/** Load from file the saved operations. */
	@Override
	public final void save() {
		FileWriter fw = null;
		try {
			fw = new FileWriter("inputs.txt", false);
			for (int i = 0; i < recentOperations.size() - 1; i++) {
				fw.write(recentOperations.get(i));
				fw.write("\n");
			}
			fw.write(recentOperations.getLast());
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		currentSaved = current;
	}

	/** loading from file to Linked list. */
	@Override
	public final void load() {
		recentOperations.clear();
		try {
			FileReader fr = new FileReader("inputs.txt");
			BufferedReader br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {
				recentOperations.add(line);
			}
			fr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		current = currentSaved;
	}

}
