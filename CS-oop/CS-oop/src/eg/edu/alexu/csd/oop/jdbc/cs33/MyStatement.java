package eg.edu.alexu.csd.oop.jdbc.cs33;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.ArrayList;
import java.util.LinkedList;

import eg.edu.alexu.csd.oop.db.cs33.MyDatabase;
import eg.edu.alexu.csd.oop.db.cs33.Parser;


public class MyStatement implements java.sql.Statement{
    boolean closestatment;
    Adapter adapter;
    MyResultSet resultset;
	Parser parser;
    MyDatabase dbms ;
	MyConnection connection;
	public ArrayList<String> batch;
	public LinkedList <Integer> batchcolumn;
	private int QueryTimeout = 0;
	String path,dbname;


	public MyStatement(MyConnection connection){
		this.connection=connection;
		parser= Parser.getInstance();
		dbms=new MyDatabase();
		closestatment=false;
		adapter= new Adapter();
		batchcolumn=new LinkedList <Integer>();
		this.batch = new ArrayList<String>();
		path = connection.path;
		String [] parts = path.split(System.getProperty("file.separator"));
		dbname = parts[parts.length-2];
		try {
			execute("CREATE DATABASE ".concat(dbname).concat(";"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private int startTime = (int) System.currentTimeMillis()/1000;

	@Override
	public void addBatch(String sql) throws SQLException {
		 if (closestatment) {
	            throw new SQLException();
	        }

		batchcolumn.add(0);
		 batch.add(sql);

	}

	@Override
	public void clearBatch() throws SQLException {
		 if (closestatment) {
	            throw new SQLException();
	        }
		 batchcolumn.clear();
		batch.clear();

	}

	@Override
	public void close() throws SQLException {
		 if (closestatment) {
	            return;
	        }
		 closestatment= true;
          connection = null;
          batch.clear();
          batchcolumn.clear();

	}

	@Override
	public boolean execute(String sql) throws SQLException {
		if(QueryTimeout != 0){
			if((int)System.currentTimeMillis()/1000 - startTime < QueryTimeout){
				if (closestatment) {
		            throw new SQLException();
		        }
			parser.parse(sql);
			if(parser.gettype().toLowerCase().equals("select")){
				adapter.setColumns(dbms.executeQuery(sql));
				 resultset = new MyResultSet(adapter, this);
				return true;
			}

			}
		}
		else{
			if (closestatment) {
	            throw new SQLException();
	        }
		parser.parse(sql);
		if(sql.toLowerCase().contains("select")){
			adapter.setColumns(dbms.executeQuery(sql));
			 resultset = new MyResultSet(adapter, this);
			return true;
		}if(sql.toLowerCase().contains("create table")){
			dbms.executeStructureQuery(sql);
			return false;
		}
		if(sql.toLowerCase().contains("create database")){
			dbms.executeStructureQuery(sql);
			return false;
		}
		if(sql.toLowerCase().contains("drop table")){
			dbms.executeStructureQuery(sql);
			return true;
		}
		if(sql.toLowerCase().contains("drop database")){
			dbms.executeStructureQuery(sql);
			return true;
		}

		}
		return false;
	}
	@Override
	public int executeUpdate(String sql) throws SQLException {
		if(QueryTimeout != 0){
			if((int)System.currentTimeMillis()/1000 - startTime < QueryTimeout){


			String type=parser.gettype().toLowerCase();
			if(type.equals("insert")||type.equals("update")||type.equals("delete")){
				int numcolumns=dbms.executeUpdateQuery(sql);
				batchcolumn.set(batch.indexOf(sql), numcolumns);
				return numcolumns;
			}
			}
		}
		else{
			if (closestatment) {
	            throw new SQLException();
	        }

		//String type=parser.gettype().toLowerCase();
		if(sql.toLowerCase().contains("insert")||sql.toLowerCase().contains("update")||sql.toLowerCase().contains("delete")){
			int numcolumns=dbms.executeUpdateQuery(sql);

	        if(batch.contains(sql)){
	        	batchcolumn.set(batch.indexOf(sql), numcolumns);
			}
			return numcolumns;
		}
		}
		return 0;
	}

	@Override
	public int[] executeBatch() throws SQLException {
		 if (closestatment) {
	            throw new SQLException();
	        }
		int [] excutebatch=new int[batch.size()];
		for(int i=0;i<batch.size();i++){
			excutebatch[i]=batchcolumn.get(i);
		}
		return excutebatch;
	}


	@Override
	public MyResultSet executeQuery(String sql) throws SQLException {
		if(QueryTimeout != 0){
			if((int)System.currentTimeMillis()/1000 - startTime < QueryTimeout){
				if (closestatment) {
		            throw new SQLException();
		        }

			    parser.parse(sql);
				if(parser.gettype().toLowerCase().equals("select")){
					adapter.setColumns(dbms.executeQuery(sql));
					 resultset = new MyResultSet(adapter, this);
				}else{
					 throw new SQLException();
				}
			}
		}
		else{
			if (closestatment) {
	            throw new SQLException();
	        }

		    parser.parse(sql);
			if(parser.gettype().toLowerCase().equals("select")){
				adapter.setColumns(dbms.executeQuery(sql));
				 resultset = new MyResultSet(adapter, this);
			}else{
				 throw new SQLException();
			}
		}

		return resultset;
	}

	@Override
	public Connection getConnection() throws SQLException {
		if (closestatment) {
            throw new SQLException();
        }
		return connection;
	}
	@Override
	public int getQueryTimeout() throws SQLException {

		return QueryTimeout;
	}

	@Override
	public void setQueryTimeout(int arg0) throws SQLException {
		QueryTimeout = arg0;

	}


	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void cancel() throws SQLException {
		// TODO Auto-generated method stub

	}



	@Override
	public void clearWarnings() throws SQLException {
		// TODO Auto-generated method stub

	}


	@Override
	public void closeOnCompletion() throws SQLException {
		// TODO Auto-generated method stub

	}



	@Override
	public boolean execute(String arg0, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean execute(String arg0, int[] arg1) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean execute(String arg0, String[] arg1) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public int executeUpdate(String arg0, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int executeUpdate(String arg0, int[] arg1) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int executeUpdate(String arg0, String[] arg1) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}



	@Override
	public int getFetchDirection() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getFetchSize() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ResultSet getGeneratedKeys() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getMaxFieldSize() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxRows() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean getMoreResults() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getMoreResults(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public ResultSet getResultSet() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getResultSetConcurrency() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getResultSetHoldability() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getResultSetType() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getUpdateCount() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isCloseOnCompletion() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isClosed() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isPoolable() throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setCursorName(String arg0) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setEscapeProcessing(boolean arg0) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setFetchDirection(int arg0) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setFetchSize(int arg0) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setMaxFieldSize(int arg0) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setMaxRows(int arg0) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setPoolable(boolean arg0) throws SQLException {
		// TODO Auto-generated method stub

	}



}