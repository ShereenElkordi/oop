package eg.edu.alexu.csd.oop.draw.cs33;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.HashMap;

/**
 * @author omid
 *
 */
public class Triangle extends MyShape {

	public Triangle() {
		this.properties = new HashMap<>();
		this.properties.put("x1", 0.0);
		this.properties.put("y1", 0.0);
		this.properties.put("x2", 0.0);
		this.properties.put("y2", 0.0);
		this.properties.put("x3", 0.0);
		this.properties.put("y3", 0.0);
	}

	@Override
	public void draw(Graphics canvas) {

		((Graphics2D) canvas).setColor(getFillColor());
		((Graphics2D) canvas).fillPolygon(
				new int[] { properties.get("x1").intValue(), this.properties.get("x2").intValue(),
						this.properties.get("x3").intValue() },
				new int[] { this.properties.get("y1").intValue(), this.properties.get("y2").intValue(),
						this.properties.get("y3").intValue() },
				3);

		((Graphics2D) canvas).setStroke(new BasicStroke(2));
		((Graphics2D) canvas).setColor(getColor());
		((Graphics2D) canvas).drawPolygon(
				new int[] { properties.get("x1").intValue(), this.properties.get("x2").intValue(),
						this.properties.get("x3").intValue() },
				new int[] { this.properties.get("y1").intValue(), this.properties.get("y2").intValue(),
						this.properties.get("y3").intValue() },
				3);

	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		MyShape r = new Triangle();
		r.setColor(this.color);
		r.setFillColor(this.fillcolor);
		r.setPosition(this.position);
		r.setProperties(this.properties);
		return r;
	}

}
