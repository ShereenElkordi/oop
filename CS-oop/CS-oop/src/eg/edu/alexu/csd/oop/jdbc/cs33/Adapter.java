package eg.edu.alexu.csd.oop.jdbc.cs33;

import java.util.ArrayList;

public class Adapter {

	public Object[][] selectcolumns;
	private String tableName;
	private ArrayList<String> columnNames;
	private ArrayList<String> columnTypes;

	public Adapter() {

	}

	public void setColumns(Object[][] selectcolumns) {
		this.selectcolumns = selectcolumns;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public void setColumnNames(ArrayList<String> columnNames) {
		this.columnNames = columnNames;
	}
	public void setColumnTypes(ArrayList<String> columnTypes) {
		this.columnTypes = columnTypes;
	}

	public Object[][] gettselectcolumns() {
		return this.selectcolumns;
	}

	public String gettablename() {
		return this.tableName;
	}

	public ArrayList<String> getcolumnNames() {
		return this.columnNames;
	}

	public ArrayList<String> getcolumnTypes() {
		return this.columnTypes;
	}

}