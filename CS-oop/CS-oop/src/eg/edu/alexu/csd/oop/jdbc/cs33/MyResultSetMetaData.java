package eg.edu.alexu.csd.oop.jdbc.cs33;

import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

public class MyResultSetMetaData implements java.sql.ResultSetMetaData{

	private String tableName;
    private ArrayList<String> columnNames;
    private ArrayList<String> columnTypes;

    public MyResultSetMetaData(String tableName, ArrayList<String> columnNames, ArrayList<String> columnTypes) {
    	 this.tableName = tableName;
         this.columnNames = columnNames;
         this.columnTypes = columnTypes;
    }

    @Override
	public int getColumnCount() throws SQLException {
    	return this.columnNames.size();
	}
    @Override
	public String getColumnLabel(int colum) throws SQLException {
		colum--;
    	if(colum>=0&&colum<this.columnNames.size()){
			return this.columnNames.get(colum);
		}
		return null;
	}

    @Override
	public String getColumnName(int colum) throws SQLException {

		return this.getColumnLabel(colum);
	}

	@Override
	public int getColumnType(int colum) throws SQLException {
		colum--;

		if(colum>=0&&colum<this.columnNames.size()){
    	String type=this.columnTypes.get(colum);
    	if(type.toLowerCase().equals("int")){
    		 return Types.INTEGER;

    	}else if(type.toLowerCase().equals("varchar")){
    		 return Types.VARCHAR;
    	}

		}else{

		}
			 throw new SQLException();////////


	}
	@Override
	public String getTableName(int arg0) throws SQLException {

		return this.tableName;
	}


	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCatalogName(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getColumnClassName(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public int getColumnDisplaySize(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}




	@Override
	public String getColumnTypeName(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPrecision(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getScale(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getSchemaName(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean isAutoIncrement(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCaseSensitive(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCurrency(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDefinitelyWritable(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int isNullable(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isReadOnly(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSearchable(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSigned(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isWritable(int arg0) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}